jQuery(function ($) {
    var info_message,
        url,
        gwidth,
        targets;

    // overlay speaker agreement
    // $('#agreement-link').prepOverlay({
    //     subtype: 'ajax',
    //     filter: '#content'

    // });

    // if not site admin, redirect to speaker folder when
    // a success message is displayed
    info_message = $('body:not(.userrole-site) .portalMessage.info:not(#kssPortalMessage)').html();
    if (info_message !== null) {
        // alert(info_message);
        url = $('.speaker-redirect a').attr('href');
        if (url) {
            window.location = url;
        }
    }

    if ($("#contentview-local_roles").length === 0) {
        $("body.portaltype-general_information #edit-bar, body.portaltype-hotel_needs_request #edit-bar, body.portaltype-session #edit-bar, body.portaltype-simple_file #edit-bar, body.portaltype-affiliation_and_disclosure #edit-bar").remove();
    }

    gwidth = $("#portal-globalnav").width();
    targets = $("#portal-globalnav > li:visible > a");
    targets.width(gwidth / targets.length - 12.8);

});
